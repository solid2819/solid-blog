<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", "Front\FrontController@index")->name("index");

Route::get("post/{slug}", "Front\FrontController@single_post")->name("post");

Route::get("category/{slug}", "Front\FrontController@category_post")->name("category_post");

Route::get("tag/{slug}", "Front\FrontController@tag_post")->name("tag_post");





Route::group(["middleware" => ["auth"]], function(){

	Route::get("/dashboard" , 		"Admin\AdminController@admin")->name("dashboard");
	Route::resource("tags", 		"Admin\TagController");
	Route::resource("categories", 	"Admin\CategoryController");
	Route::resource("posts", 		"Admin\PostController");

	// //Categories Routes
	// Route::resource("dashboard/tags", 		"Admin\TagController");
	// Route::get("categories", "Admin\CategoryController")->name("categories");
	// Route::resource("dashboard/posts", 		"Admin\PostController");

	// Route::match(["get","post"], "dashboard-admin-ecommerce/create-category-product", "Dashboard\Product\CategoryController@createCategory")->name("create-category-product");
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
