<!DOCTYPE html>
<html lang="en">
<head>
<title>{{ $post->name }}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Demo project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ asset("front/styles/bootstrap4/bootstrap.min.css") }}">
<link href="{{ asset("front/plugins/font-awesome-4.7.0/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset("front/plugins/OwlCarousel2-2.2.1/owl.carousel.css") }}">
<link rel="stylesheet" type="text/css" href="{{ asset("front/plugins/OwlCarousel2-2.2.1/owl.theme.default.css") }}">
<link rel="stylesheet" type="text/css" href="{{ asset("front/plugins/OwlCarousel2-2.2.1/animate.css") }}">
<link rel="stylesheet" type="text/css" href="{{ asset("front/plugins/jquery.mb.YTPlayer-3.1.12/jquery.mb.YTPlayer.css") }}">
<link rel="stylesheet" type="text/css" href="{{ asset("front/styles/post.css") }}">
<link rel="stylesheet" type="text/css" href="{{ asset("front/styles/post_responsive.css") }}">
</head>
<body>

<div class="super_container">

	<!-- Header -->
	@extends("layouts.front.header")
	
	<!-- Home -->

	<div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset("front/images/post.jpg") }}" data-speed="0.8"></div>
		<div class="home_content">
			
			<div class="post_title"><h1>{{ $post->name }}</h1></div>
		</div>
	</div>
	
	<!-- Page Content -->

	<div class="page_content">
		<div class="container">
			<div class="row row-lg-eq-height">

				<!-- Post Content -->

				<div class="col-lg-9">
					<div class="post_content">

						<!-- Top Panel -->
						<div class="post_panel post_panel_top d-flex flex-row align-items-center justify-content-start">
							<div class="author_image">
								<div>
									<img src="{{ asset('front/images/author.jpg') }}" alt="">	
								</div>
							</div>
							<div class="post_meta">
								<a href="#" class="text-dark">{{ $post->user->name }}</a>
								<span>{{ $post->created_at }}</span>
								<a href=""><span> <b>{{ $post->category->name }}</b></span></a>
							</div>
							<div class="post_share ml-sm-auto">
								<span>share</span>
								<ul class="post_share_list">
									<li class="post_share_item"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li class="post_share_item"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li class="post_share_item"><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>

						<!-- Post Body -->

						<div class="post_body">
							<p class="post_p">{!! $post->description !!}</p>
							<figure>
								<img src="front/images/post_image.jpg" alt="">
								<figcaption>Etiquetas</figcaption>
							</figure>

							<!-- Post Tags -->
							<div class="post_tags">
								<ul>
									@foreach($post->tags as $tag)
										<li class="post_tag"><a href="{{ route("tag_post", $tag->slug) }}">#{{ $tag->name }}</a></li>
									@endforeach
								</ul>
							</div>
						</div>
						
						<!-- Bottom Panel -->
						<div class="post_panel bottom_panel d-flex flex-row align-items-center justify-content-start">
							<div class="author_image"><div><img src="{{ asset('front/images/author.jpg') }}" alt=""></div></div>
							<div class="post_meta"><a href="#">{{ $post->user_id }}</a><span>{{ $post->created_at }}</span></div>
							<div class="post_share ml-sm-auto">
								<span>share</span>
								<ul class="post_share_list">
									<li class="post_share_item"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li class="post_share_item"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li class="post_share_item"><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>

						<!-- Similar Posts -->
					</div>
				</div>

				<!-- Sidebar -->

				<div class="col-lg-3">
					<div class="sidebar">
						<div class="sidebar_background"></div>

						@include("layouts.front.sidebar")		
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Footer -->

	@include("layouts.front.footer")
</div>

<script src="{{ asset("front/js/jquery-3.2.1.min.js") }}"></script>
<script src="{{ asset("front/styles/bootstrap4/popper.js") }}"></script>
<script src="{{ asset("front/styles/bootstrap4/bootstrap.min.js") }}"></script>
<script src="{{ asset("front/plugins/OwlCarousel2-2.2.1/owl.carousel.js") }}"></script>
<script src="{{ asset("front/plugins/easing/easing.js") }}"></script>
<script src="{{ asset("front/plugins/masonry/masonry.js") }}"></script>
<script src="{{ asset("front/plugins/parallax-js-master/parallax.min.js") }}"></script>
<script src="{{ asset("front/js/post.js") }}"></script>
</body>
</html>