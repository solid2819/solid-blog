<!DOCTYPE html>
<html lang="en">
<head>
<title>Vision</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Demo project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ asset("front/styles/bootstrap4/bootstrap.min.css") }}">
<link href="{{ asset('front/plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('front/plugins/OwlCarousel2-2.2.1/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('front/plugins/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('front/plugins/OwlCarousel2-2.2.1/animate.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('front/plugins/jquery.mb.YTPlayer-3.1.12/jquery.mb.YTPlayer.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('front/styles/main_styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('front/styles/responsive.css') }}">
@yield("styles")
</head>
<body>

<div class="super_container">

	<!-- Header -->
	@extends("layouts.front.header")
	
	<!-- Home -->
	<div>
		@yield("carousel")
	</div>
	
	<!-- Page Content -->

	<div class="page_content">
		<div class="container">
			<div class="row row-lg-eq-height">

				<!-- Main Content -->

				<div class="col-lg-9">
					<div class="main_content">

						<!-- Blog Section -->
						<div class="blog_section">

							@yield("first-content")
						</div>

						<!-- Blog Section - Videos -->

						

						<!-- Blog Section - Latest -->

						<div class="blog_section">
							
							@yield("second-content")
						</div>

					</div>
					<!-- <div class="load_more">
						<div id="load_more" class="load_more_button text-center trans_200">Load More</div>
					</div> -->
				</div>

				<!-- Sidebar -->

				<div class="col-lg-3">
					<div class="sidebar">
						

						@yield("sidebar")
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Footer -->

	@include("layouts.front.footer")
</div>

<script src="{{ asset('front/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('front/styles/bootstrap4/popper.js') }}"></script>
<script src="{{ asset('front/styles/bootstrap4/bootstrap.min.js') }}"></script>
<script src="{{ asset('front/plugins/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
<script src="{{ asset('front/plugins/jquery.mb.YTPlayer-3.1.12/jquery.mb.YTPlayer.js') }}"></script>
<script src="{{ asset('front/plugins/easing/easing.js') }}"></script>
<script src="{{ asset('front/plugins/masonry/masonry.js') }}"></script>
<script src="{{ asset('front/plugins/masonry/images_loaded.js') }}"></script>
<script src="{{ asset('front/js/custom.js') }}"></script>
@yield("scripts")

</body>
</html>