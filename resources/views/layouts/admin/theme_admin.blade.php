<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Panel de Administración Vision</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{ asset('admin/css/bootstrap.min.css')}} " />
    <link rel="stylesheet" href="{{ asset('admin/css/bootstrap-responsive.min.css')}} " />
    <link rel="stylesheet" href="{{ asset('admin/css/fullcalendar.css')}} " />
    <link rel="stylesheet" href="{{ asset('admin/css/matrix-style.css')}} " />
    <link rel="stylesheet" href="{{ asset('admin/css/matrix-media.css')}} " />
    <link href="{{ asset('admin/font-awesome/css/font-awesome.css')}} " rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    @yield("styles")
  </head>
  <body>

    @include("layouts.admin.header")

    <!--sidebar-menu-->
    @include("layouts.admin.sidebar")

    <!--content-->
    <div id="content">
      @yield("content")
    </div>

    <!--Footer-part-->
    @include("layouts.admin.footer")
    @yield("scripts")
  </body>
</html>
