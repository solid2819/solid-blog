		@if(session("info"))
	        <div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
	              <h4 class="alert-heading">¡Hecho!</h4>
	              {{ session("info") }}. 
	        </div>
        @endif

        @if(count($errors))
        	 <div class="alert alert-danger alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
	              <h4 class="alert-heading">¡Error!</h4>
	               <ul>
		               	@foreach($errors->all() as $error)
		               		<li>{{ $error }}</li>
		                @endforeach
	               </ul>
	        </div>
        @endif