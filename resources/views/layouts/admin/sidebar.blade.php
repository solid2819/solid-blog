<div id="sidebar">
  <a href="{{ route("dashboard") }}" class="visible-phone">
    <i class="icon icon-home"></i> Dashboard
  </a>
  <ul>
    <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
      <a href="{{ route("dashboard") }}">
        <i class="icon icon-home"></i> <span>Dashboard</span>
      </a> 
    </li>

    <li class="submenu {{ Request::is('posts') ? 'active' : '' }}"> 
      <a href="#"><i class="icon icon-th-list"></i> 
        <span>Posts</span>
      </a>
      <ul>
        <li class="{{ Request::is('posts') ? 'active' : '' }}">
          <a href="{{ route("posts.index")}}">Ver Posts</a>
        </li>
        <li class="{{ Request::is('posts.create') ? 'active' : '' }}">
          <a href="{{ route("posts.create")}}">Crear Post</a>
        </li>
      </ul>
    </li>

   <li class="submenu {{ Request::is('categories') ? 'active' : '' }}">
      <a href="#"><i class="icon icon-sitemap"></i> 
        <span>Categorías</span>
      </a>
      <ul>
        <li class="{{ Request::is('categories') ? 'active' : '' }}">
          <a href="{{ route("categories.index")}}">
            <i class="fas fa-bullseye"></i> Ver categorías
          </a>
      </li>
        <li class="{{ Request::is('categories.create') ? 'active' : '' }}">
          <a href="{{ route("categories.create") }}">Crear Categoría</a>
        </li>
      </ul>
    </li>

    <li class="submenu {{ Request::is('tags') ? 'active' : '' }}"> 
      <a href="#"><i class="icon icon-th-list"></i> 
        <span>Etiquetas</span>
      </a>
      <ul>
        <li class="{{ Request::is('tags') ? 'active' : '' }}">
          <a href="{{ route("tags.index")}}">Ver Etiquetas</a>
        </li>
        <li class="{{ Request::is('tags.create') ? 'active' : '' }}">
          <a href="{{ route("tags.create")}}">Crear Etiqueta</a>
        </li>
      </ul>
    </li>
    
    
    
  
    <li class="content"> <span>Monthly Bandwidth Transfer</span>
      <div class="progress progress-mini progress-danger active progress-striped">
        <div style="width: 77%;" class="bar"></div>
      </div>
      <span class="percent">77%</span>
      <div class="stat">21419.94 / 14000 MB</div>
    </li>
    <li class="content"> <span>Disk Space Usage</span>
      <div class="progress progress-mini active progress-striped">
        <div style="width: 87%;" class="bar"></div>
      </div>
      <span class="percent">87%</span>
      <div class="stat">604.44 / 4000 MB</div>
    </li>
  </ul>
</div>