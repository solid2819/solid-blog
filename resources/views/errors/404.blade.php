@extends("layouts.admin.theme_admin")

@section("content")

	<div id="content">
	  <div class="container-fluid">
	    <div class="row-fluid">
	      <div class="span12">
	        <div class="widget-box">
	          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
	            <h5>Error 404</h5>
	          </div>
	          <div class="widget-content">
	            <div class="error_ex">
	              <h1>404</h1>
	              <h3>Opps!, lo sentimos.</h3>
	              <p>La página a la cual intentas acceder no existe.</p>
	              <a class="btn btn-primary btn-big"  href="{{ route("dashboard") }}">Ir al Inicio</a> </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>

@endsection
