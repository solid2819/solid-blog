@extends("layouts.admin.theme_admin")

@section("styles")
  <link rel="stylesheet" href="{{ asset("admin/css/colorpicker.css") }}" />
  <link rel="stylesheet" href="{{ asset("admin/css/datepicker.css") }}" />
  <link rel="stylesheet" href="{{ asset("admin/css/matrix-media.css") }}" />
  <link rel="stylesheet" href="{{ asset("admin/css/bootstrap-wysihtml5.css") }}" />
  <link rel="stylesheet" href="{{ asset("admin/css/uniform.css") }}" />
  <link rel="stylesheet" href="{{ asset("admin/css/select2.css") }}" />

@endsection

@section("content")
	<div id="content-header">
        <div id="breadcrumb"> 
        	<a href="{{ route("dashboard") }}" title="Go to Home" class="tip-bottom">
            <i class="icon-home"></i> Home
          </a>
          <a href="{{ route("posts.index") }}" title="posts" class="tip-bottom">posts</a> 
           <a class="current">New</a> 
        </div>
    </div>
     {{-- Inclusión de mensajes flash --}}
    @include("layouts.admin.messages")

   {{--  Contenido de table --}}
    <div class="container-fluid">
        <div class="row-fluid">
          <div class="widget-box">
          <div class="widget-title"> 
              <span class="icon"><i class="icon-th"></i>
              </span>
              <h5>Nuevo Post</h5>
              <span class="icon">
                <a href="{{ route("posts.index") }}">
                    <button class="btn btn-warning btn-mini">
                    <i class="icon-arrow-left"></i> Volver
                  </button>
                </a>
              </span>
          </div>
        </div>   
        {!! Form::open(["route" => "posts.store"]) !!}
            @include("admin.posts.partials.form")
          {!! Form::close() !!}
        </div>
    </div>
     
@endsection

