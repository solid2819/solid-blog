<div class="">
  
{{ Form::hidden("user", auth()->user()->id) }}

<div class="form-group" style="margin-top:20px">
    {{ Form::label("category_id", "Categorías") }}
    {{ Form::select("category_id", $categories, null , ["class" => "form-control"])}}
</div>
 
<div class="form-group" style="margin-top:20px">
  {{ Form::label("name", "Titulo del Post") }}
  {{ Form::text("name",null, ["class" => "form-control", "id" => "name"])}} 
</div>

<div class="form-group" style="margin-top:20px">
  {{ Form::label("slug", "Slug") }}
  {{ Form::text("slug",null, ["class" => "form-control", "id" => "slug"])}} 
</div>

<div class="form-group" style="margin-top:20px">
  {{ Form::label("file", "Imagen") }}
  {{ Form::file("file") }}
</div>

<div class="form-group" style="margin-top:20px">
  {{ Form::label("status", "Estado") }}
  <label style="display: inline-block;">{{ Form::radio("status", "PUBLISHED") }} Publicado</label>

  <label style="display: inline-block;">{{ Form::radio("status", "DRAFT") }} Borrador</label>
</div>

<div class="form-group" style="margin-top:20px">
  {{ Form::label("tags", "Etiquetas")}}
  @foreach($tags as $tag)
    <label style="display: inline-block; margin-left: 10px;">
           {{ Form::checkbox("tags[]", $tag->id) }} {{$tag->name}}  
    </label>
   @endforeach
</div>

<div class="form-group" style="margin-top:20px">
  {{ Form::label("description", "Extracto") }}
  {{ Form::textarea("excerpt",null, ["class" => "form-control", "id" => "excerpt", "rows" => "2"])}} 
</div>

<div class="form-group" style="margin-top:20px">
  {{ Form::label("description", "Descripción") }}
  {{ Form::textarea("description",null, ["class" => "form-control", "id" => "description"])}} 
</div>

<div class="form-group" style="margin-top:20px">
  {{ Form::submit("Actualizar", ["class" => "btn btn-success"]) }}
  
</div>
</div>

@section("scripts")
  <script src="{{ asset("vendor/stringToSlug/jquery.stringToSlug.min.js") }} "></script>
  <script src="{{ asset("vendor/ckeditor/ckeditor.js") }} "></script>
  
  <script>
    $(document).ready(function () {
      $("#name, #slug").stringToSlug({
        callback: function (text) {
          $("#slug_tag").val(text);
        }
      });
    });

/*CK EDITOR*/
  CKEDITOR.config.height=400;
  CKEDITOR.config.width="auto";

  CKEDITOR.replace("description");
  

  </script>
@endsection
