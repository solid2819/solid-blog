@extends("layouts.admin.theme_admin")
@section("styles")
  <link rel="stylesheet" href="{{ asset("admin/css/uniform.css") }}" />
  <link rel="stylesheet" href="{{ asset("admin/css/select2.css") }}" />
@endsection

@section("content")
  <div id="content-header">
        <div id="breadcrumb"> 
          <a href="{{ route("dashboard") }}" title="Go to Home" class="tip-bottom">
            <i class="icon-home"></i> Home
          </a>
          <a class="current">Posts</a> 
        </div>
    </div>

    {{-- Inclusión de mensajes flash --}}
    @include("layouts.admin.messages")

   {{--  Contenido de table --}}
    <div class="container-fluid">
        <div class="row-fluid">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Entradas</h5>
            <span class="icon">
                <a href="{{ route("posts.create") }}">
                    <button class="btn btn-primary btn-mini">
                    <i class="icon-plus"></i> Nuevo Post
                  </button>
                </a>
              </span>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Titulo</th>
                  <th>Fecha de creación</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach($posts as $post)
                <tr class="gradeX">
                  <td>{{ $post->id }}</td>
                  <td>{{ $post->name }}</td>
                  <td>{{ $post->created_at }}</td>
                  <td>
                      <li  class="dropdown" id="action-posts" >
                        <a  href="#" data-toggle="dropdown" data-target="#action-posts" class="dropdown-toggle">
                        <i class="icon icon-list"></i>
                      </a>
                      <ul class="dropdown-menu">
                        <li class="">
                         <a href="{{ route("posts.show" , $post->id) }}" class="btn " style="border: none!important;" >
                            <i class="icon-eye-open" style="color:blue!important"></i> Ver
                          </a>
                        </li>
                        <li class="divider"></li>
                        <li class="">
                         <a class="btn" href="{{ route("posts.edit" , $post->id) }}" style="border: none!important;" >
                            <i class="icon-upload" style="color: green!important"></i> Actualizar
                          </a>
                        </li>
                        <li class="divider"></li>
                        <li class="text-center">
                          {!! Form::open(["route" => ["posts.destroy", $post->id], "method" => "DELETE"]) !!}
                           <button class="btn" style="border: none!important;" >
                              <i class="icon-trash" style="color: red!important"></i> Eliminar
                           </button>
                          {!! Form::close() !!}
                      </li>
                      </ul>
                    </li>
                  </td>
                </tr>
                @endforeach
              </tbody>{{ $posts->render() }}
            </table>
          </div> 
          
        </div>
        </div>
    </div>
@endsection