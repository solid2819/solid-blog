@extends("layouts.admin.theme_admin")

@section("styles")
  <link rel="stylesheet" href="{{ asset("admin/css/colorpicker.css") }}" />
  <link rel="stylesheet" href="{{ asset("admin/css/datepicker.css") }}" />
  <link rel="stylesheet" href="{{ asset("admin/css/matrix-media.css") }}" />
  <link rel="stylesheet" href="{{ asset("admin/css/bootstrap-wysihtml5.css") }}" />
  <link rel="stylesheet" href="{{ asset("admin/css/uniform.css") }}" />
  <link rel="stylesheet" href="{{ asset("admin/css/select2.css") }}" />

@endsection

@section("content")
	<div id="content-header">
        <div id="breadcrumb"> 
        	<a href="{{ route("dashboard") }}" title="Go to Home" class="tip-bottom">
            <i class="icon-home"></i> Home
          </a>
          <a href="{{ route("posts.index") }}" title="posts" class="tip-bottom">posts</a> 
           <a class="current">Edit <i style="text-transform: uppercase;">{{ $post->name }}</i></a> 
        </div>
    </div>
     {{-- Inclusión de mensajes flash --}}
    @include("layouts.admin.messages")

   {{--  Contenido de table --}}
    <div class="container-fluid">
        <div class="row-fluid">
          <div class="widget-box">
          <div class="widget-title"> 
              <span class="icon"><i class="icon-th"></i>
              </span>
              <h5> Editando Entrada <span style="text-transform: uppercase;">{{ $post->name }}</span></h5>
              <span class="icon">
                <a href="{{ route("posts.create") }}">
                    <button class="btn btn-primary btn-mini">
                    <i class="icon-plus"></i> Nuevo Post
                  </button>
                </a>
              </span>
              <span class="icon">
                <a href="{{ route("posts.index") }}">
                    <button class="btn btn-warning btn-mini">
                    <i class="icon-arrow-left"></i> Volver
                  </button>
                </a>
              </span>
          </div>
        </div>  
        {!! Form::model($post, ["route" => ["posts.update", $post->id], "method" => "PUT"]) !!}
            @include("admin.posts.partials.form-edit")
          {!! Form::close() !!}
        </div>
    </div>
     
@endsection