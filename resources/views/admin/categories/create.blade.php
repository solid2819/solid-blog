@extends("layouts.admin.theme_admin")


@section("content")
	<div id="content-header">
        <div id="breadcrumb"> 
        	<a href="{{ route("dashboard") }}" title="Go to Home" class="tip-bottom">
            <i class="icon-home"></i> Home
          </a>
          <a href="{{ route("categories.index") }}" title="Categories" class="tip-bottom">Categories</a> 
           <a class="current">New</a> 
        </div>
    </div>
     {{-- Inclusión de mensajes flash --}}
    @include("layouts.admin.messages")

   {{--  Contenido de table --}}
    <div class="container-fluid">
        <div class="row-fluid">
          <div class="widget-box">
          <div class="widget-title"> 
              <span class="icon"><i class="icon-th"></i>
              </span>
              <h5>Nueva Categoría</h5>
              <span class="icon">
                <a href="{{ route("categories.index") }}">
                    <button class="btn btn-warning btn-mini">
                    <i class="icon-arrow-left"></i> Volver
                  </button>
                </a>
              </span>
          </div>
        </div>   
        {!! Form::open(["route" => "categories.store"]) !!}
            @include("admin.categories.partials.form")
          {!! Form::close() !!}
        </div>
    </div>
     
@endsection

