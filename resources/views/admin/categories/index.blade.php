@extends("layouts.admin.theme_admin")
@section("styles")
  <link rel="stylesheet" href="{{ asset("admin/css/uniform.css") }}" />
  <link rel="stylesheet" href="{{ asset("admin/css/select2.css") }}" />
@endsection

@section("content")
  <div id="content-header">
        <div id="breadcrumb"> 
          <a href="{{ route("dashboard") }}" title="Go to Home" class="tip-bottom">
            <i class="icon-home"></i> Home
          </a>
          <a class="current">Categories</a> 
        </div>
    </div>

    {{-- Inclusión de mensajes flash --}}
    @include("layouts.admin.messages")

   {{--  Contenido de table --}}
    <div class="container-fluid">
        <div class="row-fluid">
          <div class="widget-box">
          <div class="widget-title"> 
              <span class="icon"><i class="icon-th"></i>
              </span>
              <h5>Categorías</h5>
              
              <span class="icon">
                <a href="{{ route("categories.create") }}">
                    <button class="btn btn-primary btn-mini">
                    <i class="icon-plus"></i> Nueva
                  </button>
                </a>
              </span>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Categoría</th>
                  <th>Creación</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach($categories as $category)
                <tr class="gradeX">
                  <td>{{ $category->id }}</td>
                  <td>{{ $category->name }}</td>
                  <td>{{ $category->created_at }}</td>
                  <td>
                      <li  class="dropdown" id="action-categories" >
                        <a  href="#" data-toggle="dropdown" data-target="#action-categories" class="dropdown-toggle">
                        <i class="icon icon-list"></i>
                      </a>
                      <ul class="dropdown-menu">
                        <li class="">
                         <a href="{{ route("categories.show" , $category->id) }}" class="btn " style="border: none!important;" >
                            <i class="icon-eye-open" style="color:blue!important"></i> Ver
                          </a>
                        </li>
                        <li class="divider"></li>
                        <li class="">
                         <a class="btn" href="{{ route("categories.edit" , $category->id) }}" style="border: none!important;" >
                            <i class="icon-upload" style="color: green!important"></i> Actualizar
                          </a>
                        </li>
                        <li class="divider"></li>
                        <li class="text-center">
                          {!! Form::open(["route" => ["categories.destroy", $category->id], "method" => "DELETE"]) !!}
                           <button class="btn" style="border: none!important;" >
                              <i class="icon-trash" style="color: red!important"></i> Eliminar
                           </button>
                          {!! Form::close() !!}
                      </li>
                      </ul>
                    </li>
                  </td>
                </tr>
                @endforeach
                
              </tbody>
            </table>
          </div>
        </div>
        </div>
    </div>
@endsection