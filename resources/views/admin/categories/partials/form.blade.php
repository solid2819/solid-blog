<div class="form-group">
  {{ Form::label("name", "Nombre de etiqueta") }}
  {{ Form::text("name",null, ["class" => "form-control", "id" => "name"])}} 
</div>

<div class="form-group">
  {{ Form::label("slug", "Slug") }}
  {{ Form::text("slug",null, ["class" => "form-control", "id" => "slug"])}} 
</div>

<div class="form-group">
  {{ Form::label("description", "Descripción") }}
  {{ Form::textarea("description",null, ["class" => "form-control", "id" => "description"])}} 
</div>

<div class="form-group">
  {{ Form::submit("Crear", ["class" => "btn btn-success"]) }}
  
</div>


@section("scripts")
  <script src="{{ asset("vendor/stringToSlug/jquery.stringToSlug.min.js") }} "></script>
  
  <script>
    $(document).ready(function () {
      $("#name, #slug").stringToSlug({
        callback: function (text) {
          $("#slug_tag").val(text);
        }
      });
    });
  </script>
@endsection