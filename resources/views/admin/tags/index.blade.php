@extends("layouts.admin.theme_admin")
@section("styles")
  <link rel="stylesheet" href="{{ asset("admin/css/uniform.css") }}" />
  <link rel="stylesheet" href="{{ asset("admin/css/select2.css") }}" />
@endsection
@section("content")
  <div id="content-header">
        <div id="breadcrumb"> 
          <a href="{{ route("dashboard") }}" title="Go to Home" class="tip-bottom">
            <i class="icon-home"></i> Home
          </a>
          <a class="current">Tags</a> 
        </div>
    </div>

    {{-- Inclusión de mensajes flash --}}
    @include("layouts.admin.messages")

   {{--  Contenido de table --}}
    <div class="container-fluid">
        <div class="row-fluid">
          <div class="widget-box">
          <div class="widget-title"> 
              <span class="icon"><i class="icon-th"></i>
              </span>
              <h5>Etiquetas</h5>
              
              <span class="icon">
                <a href="{{ route("tags.create") }}">
                    <button class="btn btn-primary btn-mini">
                    <i class="icon-plus"></i> Nueva
                  </button>
                </a>
              </span>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Etiqueta</th>
                  <th>Descripción</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach($tags as $tag)
                <tr class="gradeX text-center center">
                  <td>{{ $tag->id }}</td>
                  <td>{{ $tag->name }}</td>
                  <td>n/a</td>
                  <td>
                      <li  class="dropdown" id="action-tags" >
                        <a  href="#" data-toggle="dropdown" data-target="#action-tags" class="dropdown-toggle">
                        <i class="icon icon-list"></i>
                      </a>
                      <ul class="dropdown-menu">
                        <li class="">
                         <a href="{{ route("tags.show" , $tag->id) }}" class="btn " style="border: none!important;" >
                            <i class="icon-eye-open" style="color:blue!important"></i> Ver
                          </a>
                        </li>
                        <li class="divider"></li>
                        <li class="">
                         <a class="btn" href="{{ route("tags.edit" , $tag->id) }}" style="border: none!important;" >
                            <i class="icon-upload" style="color: green!important"></i> Actualizar
                          </a>
                        </li>
                        <li class="divider"></li>
                        <li class="text-center">
                          {!! Form::open(["route" => ["tags.destroy", $tag->id], "method" => "DELETE"]) !!}
                           <button class="btn" style="border: none!important;" >
                              <i class="icon-trash" style="color: red!important"></i> Eliminar
                           </button>
                          {!! Form::close() !!}
                      </li>
                      </ul>
                    </li>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        </div>
    </div>
@endsection