@extends("layouts.admin.theme_admin")


@section("content")
	<div id="content-header">
        <div id="breadcrumb"> 
        	<a href="{{ route("dashboard") }}" title="Go to Home" class="tip-bottom">
            <i class="icon-home"></i> Home
          </a>
          <a href="{{ route("tags.index") }}" title="Tags" class="tip-bottom">Tags</a> 
           <a class="current">Edit <i style="text-transform: uppercase;">{{ $tag->name }}</i></a> 
        </div>
    </div>
     {{-- Inclusión de mensajes flash --}}
    @include("layouts.admin.messages")

   {{--  Contenido de table --}}
    <div class="container-fluid">
        <div class="row-fluid">
          <div class="widget-box">
          <div class="widget-title"> 
              <span class="icon"><i class="icon-th"></i>
              </span>
              <h5> Editando Etiqueta <span style="text-transform: uppercase;">{{ $tag->name }}</span></h5>
              <span class="icon">
                <a href="{{ route("tags.create") }}">
                    <button class="btn btn-primary btn-mini">
                    <i class="icon-plus"></i> Nueva
                  </button>
                </a>
              </span>
              <span class="icon">
                <a href="{{ route("tags.index") }}">
                    <button class="btn btn-warning btn-mini">
                    <i class="icon-arrow-left"></i> Volver
                  </button>
                </a>
              </span>
          </div>
          {!! Form::model($tag, ["route" => ["tags.update", $tag->id], "method" => "PUT"]) !!}

          {!! Form::close() !!}

        </div>  
        </div>
    </div>
     
@endsection