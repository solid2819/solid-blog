@extends("layouts.front.AvisionTheme")

<!-- Carousel -->
@section("carousel")
	@extends("layouts.front.slider")
@endsection

<!-- Recent posts -->
@section("first-content")

	<div class="blog_section">
		<div class="section_panel d-flex flex-row align-items-center justify-content-start">

			<div class="section_title"><h2>Recent</h2></div>
			<!--Categories panel-->
			<div class="section_tags ml-auto">
				<ul>
					<li class="mt-1 active"><a href="category.html">all</a></li>
					<li class="mt-1"><a href="category.html">style hunter</a></li>
					<li class="mt-1"><a href="category.html">vogue</a></li>
					<li class="mt-1"><a href="category.html">health & fitness</a></li>
					<li class="mt-1"><a href="category.html">travel</a></li>
				</ul>
			</div>
			<div class="section_panel_more">
				<ul>
					<li>More
						<ul>
							<li><a href="category.html">new look 2018</a></li>
							<li><a href="category.html">street fashion</a></li>
							<li><a href="category.html">business</a></li>
							<li><a href="category.html">recipes</a></li>
							<li><a href="category.html">sport</a></li>
							<li><a href="category.html">celebrities</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<!--End Categories panel-->
		</div>
	</div>

	<div class="section_content text-center">
		@foreach($posts as  $post)
			<div class="grid clearfix">
				<!-- Largest Card With Image -->
				<div class="card card_largest_with_image grid-item">

					@if($post->file)
						<a href="{{ route("post", $post->slug) }}"><img class="card-img-top" src="{{ asset("front/images/post_1.jpg") }}{{-- {{ $post->file }} --}}" alt="Image "></a>
					@endif
					<div class="card-body">
						<div class="card-title">
							<a href="{{ route("category_post",$post->category->slug) }}">
								{{ $post->category->name }}
							</a>
							<a href="{{ route("post", $post->slug) }}">
								<h3>{{ $post->name }}</h3>
							</a>
						</div>
						<p class="card-text">{{ $post->excerpt }}</p>
						<small class="post_meta text-dark">Autor: <a href="#" class="text-dark">{{ $post->user->name }}</a><span>{{ $post->created_at }}</span></small>

						<a class="card-text" href="{{ route("post", $post->slug) }}">Learn More</a>
					</div>
				</div>

				<!-- Small Card Without Image -->
				{{-- <div class="card card_default card_small_no_image grid-item">
					<div class="card-body">
						<div class="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
						<small class="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
					</div>
				</div> --}}

				<!-- Small Card With Background -->
				 <div class="card card_default card_small_with_background grid-item">
					{{-- <div class="card_background" style="background-image:url(front/images/post_4.jpg)"></div>
					<div class="card-body">
						<div class="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
						<small class="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
					</div> --}}
				</div>
				
			</div>
		@endforeach
		{{ $posts->render() }}
	</div>
@endsection

<!-- Popular Videos -->
@section("firts-content-2")

@endsection()


<!-- Lastest Posts -->
{{-- @section("second-content")
	<div class="section_panel d-flex flex-row align-items-center justify-content-start">
		<div class="section_title"><h2>Latest Articles</h2></div>
	</div>

						<div class="section_content">
								<div class="grid clearfix">
									
									<!-- Small Card With Image -->
									<div class="card card_small_with_image grid-item">
										<img class="card-img-top" src="front/images/post_10.jpg" alt="">
										<div class="card-body">
											<div class="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
											<small class="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
										</div>
									</div>

									<!-- Small Card Without Image -->
									<div class="card card_default card_small_no_image grid-item">
										<div class="card-body">
											<div class="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
											<small class="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
										</div>
									</div>

									<!-- Small Card With Image -->
									<div class="card card_small_with_image grid-item">
										<img class="card-img-top" src="front/images/post_15.jpg" alt="">
										<div class="card-body">
											<div class="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
											<small class="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
										</div>
									</div>

									<!-- Small Card With Image -->
									<div class="card card_small_with_image grid-item">
										<img class="card-img-top" src="front/images/post_13.jpg" alt="https://unsplash.com/@jakobowens1">
										<div class="card-body">
											<div class="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
											<small class="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
										</div>
									</div>

									<!-- Small Card With Background -->
									<div class="card card_default card_small_with_background grid-item">
										<div class="card_background" style="background-image:url(front/images/post_11.jpg)"></div>
										<div class="card-body">
											<div class="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
											<small class="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
										</div>
									</div>

									Small Card With Background
									<div class="card card_default card_small_with_background grid-item">
										<div class="card_background" style="background-image:url(front/images/post_16.jpg)"></div>
										<div class="card-body">
											<div class="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
											<small class="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
										</div>
									</div>

									<!-- Small Card With Image -->
									<div class="card card_small_with_image grid-item">
										<img class="card-img-top" src="front/images/post_14.jpg" alt="">
										<div class="card-body">
											<div class="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
											<small class="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
										</div>
									</div>

									<!-- Small Card Without Image -->
									<div class="card card_default card_small_no_image grid-item">
										<div class="card-body">
											<div class="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
											<small class="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
										</div>
									</div>

									<!-- Small Card Without Image -->
									<div class="card card_default card_small_no_image grid-item">
										<div class="card-body">
											<div class="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</a></div>
											<small class="post_meta"><a href="#">Katy Liu</a><span>Sep 29, 2017 at 9:48 am</span></small>
										</div>
									</div>

									<!-- Default Card With Background -->
									<div class="card card_default card_default_with_background grid-item">
										<div class="card_background" style="background-image:url(front/images/post_12.jpg)"></div>
										<div class="card-body">
											<div class="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most</a></div>
										</div>
									</div>

									<!-- Default Card With Background -->
									<div class="card card_default card_default_with_background grid-item">
										<div class="card_background" style="background-image:url(front/images/post_6.jpg)"></div>
										<div class="card-body">
											<div class="card-title card-title-small"><a href="post.html">How Did van Gogh’s Turbulent Mind Depict One of the Most</a></div>
										</div>
									</div>
								</div>
								
							</div>
@endsection --}}


<!-- Sidebar -->
@section("sidebar")
	@include("layouts.front.sidebar")
@endsection