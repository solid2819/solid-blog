<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Post;
use App\Category;

class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // Return View First Page
    public function index()
    {
        // 
        $posts = Post::orderBy("id","DESC")->where("status","PUBLISHED")->paginate(6);
        
        
        return view("front.index", compact("posts"));
    }


    //Method Single Post
    public function single_post($slug)
    {
        $post = Post::where("slug", $slug)->first();

        return view("front.single_post", compact("post"));
    }


    public function category_post($slug)
    {
        $category = Category::where("slug", $slug)->pluck("id")->first();
        $posts    = Post::where("category_id", $category)
        ->orderBy("id","DESC")->where("status","PUBLISHED")->paginate(6);

        return view("front.category", compact("posts", "category"));
    }

    public function tag_post($slug)
    {
       
        $posts    = Post::whereHas("tags", function ($query) use($slug)
        {
            $query->where("slug", $slug);
        })->orderBy("id","DESC")->where("status","PUBLISHED")->paginate(6);

        return view("front.index", compact("posts"));
    }








    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
