<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUpdateRequest;

use App\Http\Controllers\Controller;
use App\Category;
use App\Tag;
use App\Post;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy("id", "DESC")->where("user_id", auth()->user()->id)->paginate(10); 
        //Me permite listar los articulos pertenecientes por usuario determinado

        return view("admin.posts.index", compact("posts"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy("name", "ASC")->pluck("name", "id");
        $tags = Tag::orderBy("name", "ASC")->get();

        return view("admin.posts.create", compact("categories", "tags"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request)
    {
        $post = Post::create($request->all());

        return redirect()->route("posts.edit",$post->id)->with("info","Post creado con éxito");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $categories = Category::orderBy("name", "ASC")->pluck("name", "id");
        $tags = Tag::orderBy("name", "ASC")->get();
        $post = Post::find($id);

        return view("admin.posts.show", compact("post", "categories", "tags"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::orderBy("name", "ASC")->pluck("name", "id");
        $tags = Tag::orderBy("name", "ASC")->get();
        $post = Post::find($id);

        return view("admin.posts.edit", compact("post", "categories", "tags"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Post $request, $id)
    {
        $post = Post::find($id);

        $post->file($request->all()->save());

         return redirect()->route("posts.edit",$post->id)->with("info","Post creado con éxito");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id)->delete();

        return back()->with("info", "El Post ha sido eliminada" );
    }
}
